# CMake 3.24 is the first version to be able to detect LLVMFlang.
cmake_minimum_required(VERSION 3.24)

project(LLVMExample)

option(EX_ENABLE_FORTRAN "Build with fortran support" OFF)
option(EX_BUILD_STATIC "Build with static libraries" OFF)

set(EX_LANGUAGES C CXX)
if(EX_ENABLE_FORTRAN)
  list(APPEND EX_LANGUAGES Fortran)
endif()
enable_language(${EX_LANGUAGES})

message(STATUS "Using CXX Compiler: ${CMAKE_CXX_COMPILER}")
message(STATUS "Using C Compiler: ${CMAKE_C_COMPILER}")
if(EX_ENABLE_FORTRAN)
  message(STATUS "Using Fortran Compiler: ${CMAKE_Fortran_COMPILER}")
endif()

# This lets the user specify the LLVM version to use for this test project.
# Hopefully this whole project is simple enough to be working across multiple
# versions of LLVM.
set(EX_LLVM_VERSION "15" CACHE STRING "LLVM version this should use")

find_package(LLVM ${EX_LLVM_VERSION} REQUIRED CONFIG)
list(APPEND CMAKE_MODULE_PATH "${LLVM_CMAKE_DIR}")
include(AddLLVM)

message(STATUS "Found LLVM ${LLVM_PACKAGE_VERSION} in ${LLVM_INSTALL_PREFIX}")
message(STATUS "Using LLVM binaries in: ${LLVM_TOOLS_BINARY_DIR}")

# Sanity check that the compilers we used are the one expected, since the whole
# purpose of this is to check a distribution of LLVM and its tools/frontends.
set(EXPECTED_COMPILER "Clang ${LLVM_VERSION}")
if(NOT "${CMAKE_CXX_COMPILER_ID} ${CMAKE_CXX_COMPILER_VERSION}" STREQUAL EXPECTED_COMPILER
   OR NOT "${CMAKE_C_COMPILER_ID} ${CMAKE_C_COMPILER_VERSION}" STREQUAL EXPECTED_COMPILER)
  message(FATAL_ERROR "This project needs clang/clang++ ${LLVM_VERSION}.")
endif()

if(EX_ENABLE_FORTRAN AND NOT "${CMAKE_Fortran_COMPILER_ID} ${CMAKE_Fortran_COMPILER_VERSION}" STREQUAL "LLVMFlang ${LLVM_VERSION}")
  message(FATAL_ERROR "This project needs flang ${LLVM_VERSION}")
endif()

# FIXME: Clang's cmake doesn't define a version, and *requires* LLVM.
# It seems tied to LLVM so do we mind? We may want to include Clang's version in
# there.
find_package(Clang REQUIRED CONFIG)
message(STATUS "Found Clang in ${CLANG_INSTALL_PREFIX}")
list(APPEND CMAKE_MODULE_PATH "${CLANG_CMAKE_DIR}")
# Include this for clang_target_link_libraries
include(AddClang)


# Make the LLVM definitions globally available.
add_definitions(${LLVM_DEFINITIONS})
include_directories(${LLVM_INCLUDE_DIRS})

if(EX_BUILD_STATIC)
  set(EX_LLVM_DYLIB_OPTION "DISABLE_LLVM_LINK_LLVM_DYLIB")
else()
  set(EX_LLVM_DYLIB_OPTION )
endif()

# Enable testing
include(CTest)

add_subdirectory(c)
if(EX_ENABLE_FORTRAN)
  add_subdirectory(fortran)
endif()
