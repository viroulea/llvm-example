#ifdef ENABLE_CLANG
#include "clang/Basic/DiagnosticOptions.h"
#include "clang/Driver/Driver.h"
#include "clang/Driver/ToolChain.h"
#include "clang/Frontend/CompilerInvocation.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#endif

#include "llvm/Config/llvm-config.h"
#include "llvm/InitializePasses.h"
#include "llvm/Pass.h"
#include "llvm/PassRegistry.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/InitLLVM.h"
#include "llvm/Support/VirtualFileSystem.h"

using namespace llvm;
namespace {
cl::opt<bool> ShowMessage("show-message", cl::desc("Show the message."));
struct ExampleListener : PassRegistrationListener {
  void passEnumerate(PassInfo const *PI) override {
    outs() << PI->getPassName() << "\n";
  }
};

std::string getExecutablePath(const char *arg) {
  // This just needs to be some symbol in the binary; C++ doesn't
  // allow taking the address of ::main however.
  void *P = (void *)(intptr_t)getExecutablePath;
  return sys::fs::getMainExecutable(arg, P);
}
} // namespace

int main(int Argc, char **Argv) {
  std::string Path = getExecutablePath(Argv[0]);
  InitLLVM X(Argc, Argv);
  SmallVector<const char *, 256> Args(Argv, Argv + Argc);

  cl::ParseCommandLineOptions(Argc, Argv, "A simple example program with LLVM");

  if (ShowMessage) {
    outs() << "This is LLVM " << LLVM_VERSION_STRING << "\n";
  }

  PassRegistry &Registry = *PassRegistry::getPassRegistry();
  initializeCore(Registry);
  outs() << "Passes registered so far:\n";
  ExampleListener EL;
  Registry.enumerateWith(&EL);

  // Very basic test

#ifdef ENABLE_CLANG
  using namespace clang;

  IntrusiveRefCntPtr<DiagnosticOptions> DiagOpts =
      CreateAndPopulateDiagOpts(Args);

  TextDiagnosticPrinter *DiagClient =
      new TextDiagnosticPrinter(errs(), &*DiagOpts);

  IntrusiveRefCntPtr<DiagnosticIDs> DiagID(new DiagnosticIDs());

  DiagnosticsEngine Diags(DiagID, &*DiagOpts, DiagClient);
  driver::Driver TheDriver(Path, sys::getDefaultTargetTriple(), Diags);
  outs() << "Driver path: " << TheDriver.getClangProgramPath() << "\n";
#endif

  return 0;
}
